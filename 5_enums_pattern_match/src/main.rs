enum IpAddrKind {
    V4(u8, u8, u8, u8),
    V6,
}

struct IpAddr {
    kind: IpAddrKind,
    address: String,
}

enum Message {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(i32, i32, i32),
}

// enum function
impl Message {
    fn some_function() {
        println!("test")
    }
}
fn main() {
    let four = IpAddrKind::V4;
    let six = IpAddrKind::V6;

    let localhost = IpAddrKind::V4(127, 0, 0, 1);

    // option enum
    let some_number = Some(5);
    let some_string = Some("a string");

    let absence_of_number: Option<i32> = None;

    //
    let x: i8 = 5;
    let y: Option<i8> = None;

    let sum = x + y.unwrap_or(0);
}
