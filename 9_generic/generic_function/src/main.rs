fn main() {
    let number_list = vec![34, 50, 25, 100, 65];
    let largest_number = get_largest(number_list);
    println!("The largest number is: {}", largest_number);

    let char_list = vec!['y', 'z', 'a', 'q'];
    let largest_char = get_largest(char_list);
    println!("The largest char is: {}", largest_char);

    generic_struct();
}

fn get_largest<T: PartialOrd + Copy>(number_list: Vec<T>) -> T {
    let mut largest = number_list[0];
    for number in number_list {
        if number > largest {
            largest = number
        }
    }
    largest
}

struct Point<T> {
    x: T,
    y: T,
}

impl<U> Point<U> {
    fn x(&self) -> &U {
        &self.x
    }
}

impl Point<f64> {
    fn y(&self) -> &f64 {
        &self.x
    }
}

struct Point2<T, U> {
    x: T,
    y: U,
}

impl<T, U> Point2<T, U> {
    fn mixup<V, W>(self, other: Point2<V, W>) -> Point2<T, W> {
        Point2 {
            x: self.x,
            y: other.y,
        }
    }
}

fn generic_struct() {
    let p = Point { x: 5, y: 10 };
    p.x();
    let p2 = Point { x: 5.0, y: 10.0 };
    p2.y();

    let p3 = Point2{x:5, y:10.4};
    let p4 = Point2{x:"Hello", y:'c'};

    let p5 = p3.mixup(p4);

    println!("p5 x={}, p5 y={}", p5.x, p5.y);
    println!("p5 x={}, p5 y={}", p5.x, p5.y);
}
