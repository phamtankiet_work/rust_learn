// Attribute-like macros

#[route(GET, "/")]
fn index() {
    println!("Hello, world!");
}

#[proc_macro_attribute]
pub fn route(attr: TokenStream, item: TokenStream) -> TokenStream {
    // ...
}
fn main() {
    println!("Hello, world!");
}
