enum Color {
    Rgb(i32, i32, i32),
    Hsv(i32, i32, i32),
}
enum Massage {
    Quit,
    Move { x: i32, y: i32 },
    Write(String),
    ChangeColor(Color),
}

fn main() {
    let msg = Massage::ChangeColor(Color::Hsv(0, 160, 255));

    match msg {
        Massage::Quit => {
            println!("Quit");
        }
        Massage::Move { x, y } => {
            println!("Move x: {}, y: {}", x, y);
        }
        Massage::Write(text) => {
            println!("Write: {}", text);
        }
        Massage::ChangeColor(Color::Rgb(r, g, b)) => {
            println!("ChangeColor r: {}, g: {}, b: {}", r, g, b);
        }
        Massage::ChangeColor(Color::Hsv(h, s, v)) => {
            println!("ChangeColor hue: {}, saturation: {}, value: {}", h, s, v);
        }
    }
}
