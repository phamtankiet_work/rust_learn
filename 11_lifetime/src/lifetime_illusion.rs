fn main() {}

// 1. Each parameter that is a reference gets its own lifetime parameter ('a, 'b)

// 2. If there is exactly one input lifetime parameter ('a),
//      that lifetime is assigned to all output lifetime parameters ('a)

// 3. If there are multiple input lifetime parameters, but one of them is &self or &mut self,
//      the lifetime of self is assigned to all output lifetime parameters

fn first_word(s: &str) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}
