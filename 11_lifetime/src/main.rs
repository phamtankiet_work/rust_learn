mod generic_lifetime;
mod struct_lifetime;

fn main() {
    println!("Hello, world!");
    generic_lifetime::generical_main();
    struct_lifetime::struct_lifetime_main();
}
