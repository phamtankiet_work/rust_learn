fn main() {
    let s: &'static str = "I have a static lifetime.";
    // static lifetime live as long as the duration of the program

    // all string literal ("") have a static lifetime cause store in the binary
}
