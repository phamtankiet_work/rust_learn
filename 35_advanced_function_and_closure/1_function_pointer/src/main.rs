fn add_one(x: i32) -> i32 {
    x + 1
}

fn do_twice(f: fn(i32) -> i32, arg: i32) -> i32 {
    f(arg) + f(arg)
}

// trait bounds

fn do_twice_trait<T>(f: T, arg: i32) -> i32
where
    T: Fn(i32) -> i32,
{
    f(arg) + f(arg)
}
fn main() {
    let answer = do_twice(add_one, 5);

    println!("The answer is: {}", answer);

    let list_of_number = vec![1, 2, 3];
    let list_of_strings: Vec<String> = list_of_number.iter().map(ToString::to_string).collect();

    println!("list of strings: {:?}", list_of_strings);

    enum Status {
        Value(i32),
        Stop,
    }

    let list_of_statuses: Vec<Status> = (0u32..20).map(Status::Value).collect();
}
