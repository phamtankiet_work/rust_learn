use std::fmt::{Debug, Display};
mod blanket_implementation;
mod conditionally_implement;
pub struct NewsArticle {
    pub headline: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summarize_author(&self) -> String {
        format!("{}", self.author)
    }
}
pub struct Tweet {
    pub user: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweet {
    fn summarize_author(&self) -> String {
        format!("@{}", self.user)
    }
    fn summarize(&self) -> String {
        format!("{}: {}", self.user, self.content)
    }
}

pub trait Summary {
    fn summarize_author(&self) -> String;
    fn summarize(&self) -> String {
        format!("(Read more from {} ....)", self.summarize_author())
    }
}

// trait as parameter
pub fn notify(item: &impl Summary) {
    println!("Breaking news! {}", item.summarize());
}

// trait bound
fn some_function<T, U>(t: &T, u: &U) -> i32
where
    T: Display + Clone,
    U: Clone + Debug,
{
    1
}

// return trait
fn returns_summarizable() -> impl Summary {
    Tweet {
        user: String::from("John"),
        content: String::from("This is the content of the tweet"),
        reply: false,
        retweet: false,
    }
}
fn main() {
    let article = NewsArticle {
        headline: String::from("Hello, world!"),
        author: String::from("author name"),
        content: String::from("This is the content of the article"),
    };
    let tweet = Tweet {
        user: String::from("John"),
        content: String::from("This is the content of the tweet"),
        reply: false,
        retweet: false,
    };
    let summary = article.summarize();
    let summary2 = tweet.summarize();
    println!("Summary of article: {}", summary);
    println!("Summary of tweet: {}", summary2);
    println!("Hello, world!");

    notify(&article);
    notify(&article);
    println!("{}", returns_summarizable().summarize());
    crate::conditionally_implement::condition_main();
    blanket_implementation::blanket_main();
}
