struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}
#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}
impl Rectangle {
    //method with self
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
}

impl Rectangle {
    // associated function without self reference
    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

fn main() {
    println!("Hello, world!");
    let mut user1 = User {
        email: String::from("a@b.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    let name = user1.username;
    println!("The name is: {}", name);

    user1.username = String::from("newusername");
    println!("The name is: {}", user1.username);

    let user2 = build_user(String::from("a2@b.com"), String::from("somename123"));

    let user3 = User {
        email: String::from("a3@b.com"),
        username: String::from("somename123"),
        ..user2
    };

    println!("The name is: {}", user3.username);

    let rect = (30, 50);

    println!("The area is: {}", calculate_area(rect));

    let new_rect = Rectangle {
        width: 30,
        height: 50,
    };
    println!("The area is: {}", calculate_area_struct(&new_rect));

    println!("rect: {:#?}", new_rect);

    println!("The implementation method area is: {}", new_rect.area());

    let smaller_rectangle = Rectangle {
        width: 3,
        height: 5,
    };

    let bigger_rectangle = Rectangle {
        width: 300,
        height: 500,
    };

    println!(
        "Can new_rect hold smaller_rect? {}",
        new_rect.can_hold(&smaller_rectangle)
    );
    println!(
        "Can new_rect hold bigger_rect? {}",
        new_rect.can_hold(&bigger_rectangle)
    );

    let rect4 = Rectangle::square(30);
    println!("The area is: {}", rect4.area());
}

fn build_user(email: String, username: String) -> User {
    return User {
        email,
        username,
        active: true,
        sign_in_count: 1,
    };
}

fn calculate_area(dimensions: (u32, u32)) -> u32 {
    dimensions.0 * dimensions.1
}

fn calculate_area_struct(rectangle: &Rectangle) -> u32 {
    rectangle.width * rectangle.height
}
