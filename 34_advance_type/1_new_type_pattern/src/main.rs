use std::fmt;

struct Warpper(Vec<String>);

impl fmt::Display for Warpper {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0.join(" "))
    }
}
fn main() {
    let w = Warpper(vec!["hello".to_string(), "world".to_string()]);

    println!("{}", w);
}
