fn main() {
    while game_in_progress {
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
    }

    print!("forever ");
    loop {
        print!("and ever ");
    }
}

fn bar() -> ! {
    // --snip--
}

impl<T> Option<T> {
    pub fn unwarp(self) -> T {
        match self {
            Some(t) => t,
            None => panic!("called `Option::unwrap()` on a `None` value"),
        }
    }
}
