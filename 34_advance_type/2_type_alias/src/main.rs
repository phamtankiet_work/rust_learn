fn main() {
    type Kilometers = i32;

    let x: i32 = 5;
    let y: Kilometers = 5;

    println!("x + y = {}", x + y);

    type Thunk = Box<dyn Fn() + Send + 'static>;

    let f: Thunk = Box::new(|| println!("hi"));

    fn take_long_type(f: Thunk) {
        //-- snip
    };

    fn return_long_type() -> Thunk {
        Box::new(|| ())
    };
}
