use std::fs::File;
use std::io::ErrorKind;

fn main() {
    println!("Hello, world!");
    a();

    let f = File::open("hello.txt");

    let f = match f {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create("hello.txt") {
                Ok(fc) => fc,
                Err(e) => panic!("Problem creating the file: {:?}", e),
            },
            other_error => {
                panic!("Problem opening the file: {:?}", other_error)
            }
        },
    };
}

fn a() {
    b()
}

fn b() {
    c(21)
}

fn c(num: i32) {
    if num == 22 {
        panic!("Don't pass in 22!")
    }
}
