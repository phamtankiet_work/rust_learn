fn main() {
    // ===========================================
    //              match expression
    // ===========================================
    #[derive(Debug)]
    enum Language {
        English,
        Spanish,
        Russian,
        Japanese,
    }

    let language = Language::English;

    match language {
        Language::English => println!("Hello"),
        Language::Spanish => println!("Hola"),
        Language::Russian => println!("Privet"),
        lang => println!("I don't know that language! {:?}", lang),
    }

    // ===========================================
    //        Conditional if let Expression
    // ===========================================
    let authoriztion_status: Option<&str> = None;
    let is_admin = false;
    let group_id: Result<u8, _> = "34".parse();

    if let Some(status) = authoriztion_status {
        println!("Authorization status: {}", status);
    } else if is_admin {
        println!("Admin");
    } else if let Ok(group_id) = group_id {
        if group_id > 30 {
            println!("Authorization status: privileged");
        } else {
            println!("Authorization status: basic");
        }
    } else {
        println!("guest");
    }

    // ===========================================
    //          while let Expression
    // ===========================================

    let mut stack = Vec::new();

    stack.push(1);
    stack.push(2);
    stack.push(3);
    while let Some(top) = stack.pop() {
        println!("{}", top);
    }

    // ===========================================
    //          for loop
    // ===========================================

    let v = vec!['a', 'b', 'c'];

    for (index, value) in v.iter().enumerate() {
        println!("{} is at index {}", value, index);
    }

    // ===========================================
    //          let statements
    // ===========================================

    let x = 5;

    // let PATTERN = EXPRESSION;
    let (x, y, _) = (1, 2, 3);

    // ===========================================
    //          function parameters
    // ===========================================

    let point = (3, 5);
    print_coordinates(&point);

    // irrefutable
    let x = 5;

    // refutable
    let x: Option<&str> = None;
    if let Some(x) = x {
        println!("{}", x);
    }

    // Can only accept irrefutable patterns
    // function parameters
    // let statements
    // for loops
}

fn print_coordinates(&(x, y): &(i32, i32)) {
    println!("Current location: ({}, {})", x, y);
}
