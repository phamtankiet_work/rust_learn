//! # My crate
//!
//! `my_crate` is a collection of utilities to make performing certain
//! calculations more convenient.

/// Adds one to the number given
///
/// # Examples
///
/// ```
/// let arg = 5;
/// let answer = my_crate::add_one(arg);
///
/// assert_eq!(6, answer);
/// ```
pub fn add_one(x: i32) -> i32 {
    x + 1
}

pub use self::kinds::PrimaryColor;
pub use self::kinds::SecondaryColor;
pub use self::utils::mix;

pub mod kinds {
    // The primary colors according to the RYB color model
    pub enum PrimaryColor {
        Red,
        Yellow,
        Blue,
    }

    // The seconds colors according to the RYB color model.
    pub enum SecondaryColor {
        Orange,
        Blue,
        Purple,
    }
}

pub mod utils {
    use crate::kinds::*;

    // Combine two primary colors in equal amount to create
    // a secondary color
    pub fn mix(c1: PrimaryColor, c2: PrimaryColor) -> SecondaryColor {
        // --snip
        // ANCHOR_END: here
        SecondaryColor::Orange
        // ANCHOR: here
    }
}
