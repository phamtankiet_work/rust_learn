unsafe trait Foo {
    fn foo();
}

unsafe impl Foo for i32 {
    fn foo() {}
}

fn main() {}
