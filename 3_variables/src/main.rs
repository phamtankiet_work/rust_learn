fn main() {
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6;
    println!("The value of x is: {}", x);

    const NUM_COUNT: u32 = 100_000; // cannot mutable, must be in upper case

    let a = 98_123; // Decimal
    let b = 0xff; // Hex
    let c = 0o77; // Octal
    let d = 0b1111_1111; // Binary
    let e = b'A'; // Byte

    let f: u8 = 255; // overflow

    // Floating point number
    let g = 2.0; // f64
    let h: f32 = 3.0; // f32

    // Numeric Operations
    let sum = 5 + 10;
    let difference = 95.5 - 4.3;
    let product = 4 * 30;
    let quotient = 56.7 / 32.2;
    let remainder = 43 % 5;

    //boolean
    let t = true;
    let f: bool = false; // with explicit type annotation
    compound_type();
}

fn compound_type() {
    let tup = (500, 6.4, "123");
    let (int, float, charvar) = tup;

    // array
    let code = [200, 404, 500];

    let not_found = code[1];

    println!("The value of not_found is: {}", not_found);
}
