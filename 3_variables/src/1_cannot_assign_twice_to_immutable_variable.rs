fn main() {
    let x = 5;
    println!("The value of x is: {}", x);
    x = 6; //cannot assign twice to immutable variable
    println!("The value of x is: {}", x);
}

fn fix() {
    let mut x = 5;
    println!("The value of x is: {}", x);
    x = 6; //cannot assign twice to immutable variable
    println!("The value of x is: {}", x);
}
